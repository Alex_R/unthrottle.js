unthrottle.js - Unthrottled setTimeout in the background.

Copyright (C) Alex Richman <alex@richman.io>.

Released as free and open-source software under the ISC licence.


---

### Why?
Web browsers throttle calls to setTimeout when the tab is in the background, which tends to break timing-sensitive things like WebAudio.  unthrottle.js bounces the calls off of a WebWorker (which aren't throttled when the tab is backgrounded) so you get normal behaviour at all times.


---

### Example Usage

```javascript
    function a_callback() {
        console.log("Hello from the callback!");

        setTimeoutUnthrottled(a_callback, 100);
    }

    a_callback();
```


As with the default implimentation, you can cancel a pending timeout using the clearTimeoutUnthrottled() function:
```javascript
    var timeout_id = setTimeoutUnthrottled(function() { destroy_the_universe(); }, 999999999);

    /* Better clear that before it's too late. */
    clearTimeoutUnthrottled(timeout_id);
```


You can also replace the default functions to avoid all that extra typing (it's a drop-in replacement, so no other changes are required):
```javascript
    window.setTimeout   = setTimeoutUnthrottled;
    window.clearTimeout = clearTimeoutUnthrottled;
```
