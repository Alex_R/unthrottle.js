"use strict";


var _unthrottled_worker   = null;
var _unthrottled_requests = {};

if(window.Worker) {
    _unthrottled_worker = new Worker(window.URL.createObjectURL(new Blob([
        "onmessage = function(message) {            " +
        "    setTimeout(function() {                " +
        "        postMessage(message.data['timeout_id']);" +
        "    }, message.data['timeout']);                " +
        "}"
    ])));

    _unthrottled_worker.onmessage = function(message) {
        _unthrottled_request_handler(message.data);
    }
} else {
    throw ReferenceError("Failed to initialise web worker because the browser does not support it.");
}


function _unthrottled_allocate_request(callback, args) {
    var timeout_id = 0;

    while(timeout_id == 0 || timeout_id in _unthrottled_requests) {
        timeout_id = Math.floor((Math.random() * 999999999) + 1);
    }

    _unthrottled_requests[timeout_id] = {
        "callback": callback,
        "args":     args,
    };

    return timeout_id;
}


function _unthrottled_request_handler(timeout_id) {
    var request = _unthrottled_requests[timeout_id];
    if(request == undefined) {
        throw ReferenceError("Received invalid request ID from worker.");
    }

    if(request["callback"] != undefined) {
        request["callback"].apply(window, request["args"]);
    }

    delete _unthrottled_requests[timeout_id];
}


function setTimeoutUnthrottled(callback, timeout, ...args) {
    /* setTimeoutUnthrottled
     * An unthrottled setTimeout().
     *
     * @ func callback - A function to be called after the timeout expires.
     * @ int timeout   - The timeout (in miliseconds).
     * @ ... args      - Extra arguments to be passed to the callback function.
     *
     * # int timeout_id - The timeout ID, can be passed to clearTimeoutUnthrottled() to cancel the timeout.
     */

    if(typeof(callback) != "function") {
        throw TypeError("Argument 'callback' is not of type 'function'");
    }

    if(typeof(timeout) == "undefined") {
        timeout = 0;
    } else {
        if(typeof(timeout) != "number") {
            throw TypeError("Argument 'timeout' is not of type 'number'");
        }
    }

    if(_unthrottled_worker == null) {
        throw ReferenceError("Unthrottled worker is not initialised (check WebWorker support).");
    }

    var timeout_id = _unthrottled_allocate_request(callback, args);

    _unthrottled_worker.postMessage({
        "timeout_id": timeout_id,
        "timeout":    timeout,
    });

    return timeout_id;
}


function clearTimeoutUnthrottled(timeout_id) {
    /* clearTimeoutUnthrottled
     * Clear the timeout previously set by setTimeoutUnthrottled().
     *
     * @ int timeout_id - The ID of the timeout, as returned by setTimeoutUnthrottled().
     */

    var request = _unthrottled_requests[timeout_id];
    if(request == undefined) {
        return;
    }

    /* It's tempting to just delete the request here, but another request could feasibly be allocated with the
     * same ID, so best to just kill the callback and handle it in the request handler.
     */
    request["callback"] = undefined;
}
